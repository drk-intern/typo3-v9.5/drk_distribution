Unterstützen
============

Das Projekt kann auf verschiedene Wege unterstützt werden.

* Es können Probleme gemeldet werden
* Es können neue Features angefragt werden
* Es können Änderungen / Ergänzungen an der Dokumentation vorgenommen werden
* Es können Änderungen / Ergänzungen am Quellcode vorgenommen werden

Wichtig sind dabei folgende Anmerkungen:

* Änderungen müssen schlussendlich finanziert werden, deswegen sind kann es
  durchaus eine Weile dauern, bis die Änderungen umgesetzt sind
* Änderungen gelten immer für alle Installationen, deswegen muss auf die
  Abwährtskompatibilität geachtet werden

Bei Fragen wenden Sie sich bitte an das Projektteam der DRK-Service GmbH

Webseite
========

* https://www.drk-intern.de/musterseiten/musterseiten.html

Handbuch
========

* https://drk-intern.gitlab.io/typo3-main-distribution/Index.html
